class Nodo{
    constructor(padre = null, nivel, posicion= null, valor){
        this.padre = padre;
        this.posicion = posicion;
        this.valor = valor;
        nivel == null ? this.nivel = 0 : this.nivel = nivel + 1;
        
    }

    hijoI(padre, nivel, posicion, valor){
        var nodo = new Nodo( padre, nivel, posicion, valor);
        return nodo;
    }
    hijoD(padre, nivel, posicion, valor){
        var nodo = new Nodo(padre, nivel, posicion, valor);
        return nodo;
    }
}