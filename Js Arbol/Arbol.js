class Arbol{
    constructor(){
        this.nodoPadre = "";
        this.nodos = [];
        
    }
    agregarNodoPadre(padre, nivel, posicion, valor){
        var nodo = new Nodo(padre, nivel, posicion, valor);
        this.nodos.push(nodo);
        return nodo;
    }
    
    hijoD(nodo, valor){
        if(valor <= nodo.valor){ 
            if(nodo.hasOwnProperty('hI')){
                this.hI(nodo.hI, valor);
            }else{
                nodo.hI = new Nodo(nodo, nodo.nivel, valor, "Izquierda");
            }
        }else{
            if(nodo.hasOwnProperty('hD')){
                this.hijoD(nodo.hD, valor);
            }else{
                nodo.hD = new Nodo(nodo, nodo.nivel, valor, "Derecha");
            }
        }
        
    }

    hijoI(nodo, valor){
        if(valor <= nodo.valor){ 
            if(nodo.hasOwnProperty('hI')){
                this.hijoI(nodo.hI, valor);
            }else{
                nodo.hijoI = new Nodo(nodo, nodo.nivel, valor, "Izquierda");
            }
        }else{
            if(nodo.hasOwnProperty('hD')){
                this.hijoDd(nodo.hD, valor);
            }else{
                nodo.hD = new Nodo(nodo, nodo.nivel, valor, "Derecha");
            }
        }
        
    }

    OrdenarNodos(nodo, valor){
        if(valor <= nodo.valor){
            this.hijoI(nodo, valor);
        }else{
            this.hijoD(nodo, valor);
        }
    }
    
}